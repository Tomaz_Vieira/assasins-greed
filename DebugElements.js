function Line(point, dir, color){
  this.point = point.clone()
  this.dir = dir.clone()
  if(color != undefined){
    this.color = color
  }else{
    this.color = "#0000FF"
  }
}

Line.prototype.toEquationLine=function(){
    return this.point.toEquationLine(this.dir)
}

Line.prototype.draw=function(){
  var beginPt = this.point.sum( this.dir.scmul(2000) )
  var endPt = this.point.sum( this.dir.scmul(-2000) )

  var oldStyle = Game.context.strokeStyle
  Game.context.strokeStyle = this.color
  Game.context.setLineDash([2, 4]);
  Game.context.beginPath()
  Game.context.moveTo(beginPt.x, beginPt.y)
  Game.context.lineTo(endPt.x, endPt.y)
  Game.context.stroke()

  Game.context.strokeStyle = oldStyle
  Game.context.setLineDash([1,0])
}

function Vec(point, dir, color){
  this.point = point.clone()
  this.dir = dir.clone()
  if(color != undefined){
    this.color = color
  }else{
    this.color = "#FF0000"
  }
}

Vec.prototype.draw=function(){
  var beginPt = this.point
  var endPt = this.point.sum( this.dir )

  var oldStyle = Game.context.strokeStyle
  Game.context.strokeStyle = this.color
  Game.context.setLineDash([2, 4]);
  Game.context.beginPath()
  Game.context.moveTo(beginPt.x, beginPt.y)
  Game.context.lineTo(endPt.x, endPt.y)
  Game.context.stroke()
  
  Game.context.fillStyle = this.color
  Game.context.fillRect(endPt.x, endPt.y, 4, 4)

  Game.context.strokeStyle = oldStyle
  Game.context.setLineDash([1,0])
}
