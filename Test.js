"use strict";
function almostEqual(a, b, threshold){
  if(threshold==undefined){
    var threshold =0.0001
  }

  if(typeof(a) != typeof(b)){
    throw new Error("Comparing different types!!")
  }

  if(typeof(a) == 'number' && typeof(b) == 'number'){
    return Math.abs(a - b) < threshold
  }

  if( a instanceof Array && b instanceof Array ){
    a.validateLength(b)

    for(var i=0; i<a.length; i++){
      if( Math.abs(a[i] - b[i]) > threshold )
        return false
    }
    return true
  }

  throw new Error("Can't compare " + a + " and " + b)
}

try{
  
  //sum vectors
  var a = [123, 456]
  var b = [20, 30]
  
  var c = a.sum(b)
  if(c.x != 143 || c.y != 486){
    throw "Sum of vectors is wrong! " + a.toStr() + " plus " + b.toStr() + " should not equal " + c.toStr()
  }
  
 ///////Division//////////////// 
  var a = [120, 200]
  var d = 10
  if( ! a.div(d).almostEquals([12, 20]) ){
    throw new Error("Division of vector by scalar is messed up!")
  }
  ///////////////////////

  ///////Multi//////////////// 
  var a = [120, 200]
  var m = 3
  if( ! a.scmul(m).almostEquals([360, 600]) ){
    throw new Error("Multiplication of vector by scalar is messed up!")
  }
  ///////////////////////

  ////// magnitude /////
  var a = [1,0]
  if( !almostEqual( a.magnitude, 1 ) ){
    throw new Error( a.toStr() + " magnitude was computed as " + a.magnitude)
  }

  var a = [-1,1]
  if( !almostEqual( a.magnitude, Math.sqrt(2) ) ){
    throw new Error( a.toStr() + " magnitude was computed as " + a.magnitude)
  }

  //////// normalized /////
  var a = [20, 123]
  if( !almostEqual( a.normalized.scmul( a.magnitude ), a ) ){
    throw new Error( "Bad normalization/magnitude" )
  }

  ////// negative /////
  if( ! [123, 23].negative.equals([-123, -23]) ){
    throw new Error("Negative of vector is wrong!!")
  }

  ////// Dot ////////////
  var a = [3,7]
  var b = [8,3]
  var d = 3*8 + 3*7
  if( !almostEqual(a.dot(b), d) || !almostEqual(b.dot(a), d) ){
    throw new Error("Bad dot product")
  }

  //////// Matrix * vector /////
  var m = [
    [3,5,7],
    [2,6,4],
    [1,3,1]
  ]
  var v = [3,
           5,
           3]
  

  var mv = [(3*3 + 5*5 + 7*3),
           (2*3 + 6*5 + 4*3),
           (1*3 + 3*5 + 1*3)]
  if( ! m.mul(v).equals(mv) ){
    throw new Error("Bad matrix*vector multiplication!")
  }

  /// Rotation ///
  var v = [1,0]
  if( ! v.rotate(45).almostEquals([ 1/Math.sqrt(2), 1/Math.sqrt(2) ]) ){
    throw new Error("bad rotation!!")
  }

  if(!almostEqual( [3,17].rotate(17).magnitude, [3,17].magnitude )){
    throw new Error("Bad rotation!")
  }

  //angle
  if(!almostEqual([1,0].angle([0,1]), 90)){
    throw new Error("Bad angle calculation")
  }

   if(!almostEqual([1,0].angle([1,1]), 45)){
    throw new Error("Bad angle calculation")
  }
 

}catch(e){
  console.log(e)
}













