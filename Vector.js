"use strict";

/*
 * This is a JavaScript Scratchpad.
 *
 * Enter some JavaScript, then Right Click or choose from the Execute Menu:
 * 1. Run to evaluate the selected text (Ctrl+R),
 * 2. Inspect to bring up an Object Inspector on the result (Ctrl+I), or,
 * 3. Display to insert the result in a comment after the selection. (Ctrl+L)
 */



Object.defineProperty(
  Array.prototype,
  'x',
  {
    get: function(){ return this[0] },
    set: function(x){ this[0]=x }
  }
)

Object.defineProperty(
  Array.prototype,
  'y',
  {
    get: function(){ return this[1] },
    set: function(y){ this[1]=y }
  }
)

Object.defineProperty(
  Array.prototype,
  'z',
  {
    get: function(){ return this[2] },
    set: function(z){ this[2]=z }
  }
)

Object.defineProperty(
  Array.prototype,
  "last",
  {
    get: function(){
      return this[this.length-1]
    },
    set: function(val){
      this[this.length -1] = val
    }
  }
)

Object.defineProperty(
  Array.prototype,
  'isOneDimensional',
  {
    get: function(){
      for(var i=0; i<this.length; i++){
        if (this[i] instanceof Array){
          return false
        }
      }
      return true
    }
  }
)

Object.defineProperty(
  Array.prototype,
  "normalized",
  {
    get: function(){
      if (! this.isOneDimensional){
        throw new Error("Trying to get magnitude from matrix")
      }
      var mag = this.magnitude
      if (almostEqual(mag, 0)){
        return [0,0]
      }
      return this.clone().div(this.magnitude)
    }
  }
)

Object.defineProperty(
  Array,
  "up",
  {
    get: function(){
      return [0,-1]
    }
  }
)

Object.defineProperty(
  Array,
  "down",
  {
    get: function(){
      return [0,1]
    }
  }
)

Object.defineProperty(
  Array,
  "right",
  {
    get: function(){
      return [1,0]
    }
  }
)

Object.defineProperty(
  Array,
  "left",
  {
    get: function(){
      return [-1,0]
    }
  }
)

Object.defineProperty(
  Array.prototype,
  "magnitude",
  {
    get: function(){
      if (! this.isOneDimensional){
        throw new Error("Trying to get magnitude from matrix")
      }
      
      var sum=0;
      for(var i=0; i<this.length; i++){
        sum += this[i] * this[i]
      }
      return Math.sqrt(sum)
    }
  }
)

Object.defineProperty(
  Array.prototype,
  "negative",
  {
    get: function(){
      if (! this.isOneDimensional){
        throw new Error("Trying to get negative from matrix")
      }
      
      var neg = []
      for(var i=0; i<this.length; i++){
        neg[i] = - this[i]
      }
      return neg
    }
  }
)
//row major!

Array.prototype.validateLength = function(other){
  if(this.length != other.length){
    throw new Error( "vectors of different lengths:\nthis:\n" + this.toStr() + "\nother:\n " + other.toStr() )
  }  
}

Array.prototype.sum = function(other){
  this.validateLength(other)
  var result = []
  for(var i=0; i<this.length; i++){
    result[i] = this[i] + other[i]
  }
  return result;
}

Array.prototype.almostEquals=function(other, threshold){
  this.validateLength(other)
  if(threshold == undefined){
    threshold=0.0001
  }
  for(var i=0; i<this.length; i++){
    if( Math.abs(this[i] - other[i]) > threshold){
      return false
    }
  }
  return true
}

Array.prototype.equals=function(other){
  return this.almostEquals(other, 0)
}

Array.prototype.minus=function(other){
  return this.sum(other.negative)
}

Array.prototype.div = function(scalar){
  var result = this.clone()
  for(var i=0; i<this.length; i++){
    result[i] = this[i]/scalar
  }
  return result
}

Array.prototype.scmul = function(scalar){
  var result = this.clone()
  for(var i=0; i<this.length; i++){
    result[i] = this[i]*scalar
  }
  return result
}

Array.prototype.cross = function(other){
  console.warn("Untested cross product!!")
  var M = [
    [   0,     -this.z,   this.y],
    [ this.z,     0,     -this.x],
    [-this.y,   this.x,     0   ]
  ]
  return M.mul(other)
}

Array.prototype.dot = function(other){
  this.validateLength(other)
  var total=0;
  for(var i=0; i<this.length; i++){
    total += this[i] * other[i]
  }
  return total
}

Array.prototype.proj=function(other){
  return other.normalized.scmul( this.dot(other.normalized) )
}

Array.prototype.clone = function(){
  var c = []
  for(var i=0; i<this.length; i++){
    c[i] = this[i]
  }
  return c
}


Array.prototype.toStr = function(){
  if( this[0] instanceof Array ){
    var str="\n"
    for(var row=0; row<this.getNumRows(); row++){
      var s="|"
      for(var col=0; col<this.getNumCols(); col++){
        s+= this[row][col] + ","
      }
      s+= "|"
      str+= s + "\n"
    }
    return str
  }else{
    return JSON.stringify(this);
  }
}

Array.prototype.getNumRows = function(){
  return this.length
}

Array.prototype.getRow = function(idx){
  if(this[idx] instanceof Array){
    return this[idx].clone()
  }else{
    return this.clone()
  }
}

Array.prototype.getCol = function(idx){
  if(  this[0] instanceof Array){
    console.log("assembling columns...")
    var col = []
    for(var i=0; i<this.length; i++){
      col[i] = this[i][idx]
    }
    return col
  }else{
    return this.clone()
  }
}

Array.prototype.getNumCols = function(){
  if( this[0] instanceof Array ){
    //this is a matrix. Grab first line and return its length
    return this[0].length
  }else{
    //this is simply a vector
    return 1
  }
}

Array.prototype.Remove=function(element){
  var index = this.indexOf(element);
  if (index > -1) {
      this.splice(index, 1);
  }
 
}

Array.prototype.distanceTo=function(other){
  this.validateLength(other)
  return this.sum(other.negative).magnitude
}

Array.prototype.mul = function(other){
  if(this.isOneDimensional){
    throw new Error("Can't pre-multyiply vectors")
  }

  if(this.getNumCols() != other.getNumRows()){
    throw new Error("Can't multiply\n" +
                     this.toStr() + "\n" +
                     "wth\n" +
                      other.toStr())
  }

  var result = new Array();
  
  for(var row=0; row < this.getNumRows(); row++){
    for(var col=0; col < other.getNumCols(); col++){
      if(result[row] == undefined){
        result[row] = []
      }
      result[row][col] = this.getRow(row).dot( other.getCol(col) )
    }
  }

  //fix result array to be a simple Array instead of an array of single-element
  //arrays
  if(other.isOneDimensional){
    var fixed=[]
    for(var i=0; i<result.length; i++){
      fixed[i] = result[i][0]
    }
    return fixed
  }

  return result;
}

function degsToRads(degs){
  return (degs/180) * Math.PI
}

//rads = (degs/180) * Math.PI
//(rads/Math.PI) = degs/180
//(rads/Math.PI) * 180 = degs
function radsToDegs(rads){
  return (rads/Math.PI) * 180
}

//2d rotation around z (counter clockwise)
Array.prototype.rotate=function(degrees){
  var angle = degsToRads(degrees)
  if(this.length == 2){
    var rotM = [
      [Math.cos(angle), -Math.sin(angle)],
      [Math.sin(angle), Math.cos(angle)]
    ]
  }
  
  if(this.length == 3){
    var rotM = [
      [Math.cos(angle), -Math.sin(angle), 0],
      [Math.sin(angle),  Math.cos(angle), 0],
      [       0,              0,          1]
    ]
  }
  return rotM.mul(this)
}

Array.prototype.angle=function(/*Vector*/v){
  var rads = Math.acos( this.dot(v)/(this.magnitude * v.magnitude)  )
  return radsToDegs(rads)
}

Array.prototype.clearColumn=function(rowIdx){
  var pivIdx=0
  while(this[rowIdx][pivIdx]==0)
    pivIdx++

  for(var i=0; i<this.length; i++){
    if(i == rowIdx){
      continue
    }
    var multiplier = this[i][pivIdx]/this[rowIdx][pivIdx]
//    console.log("    Multi: " + multiplier)
    var clearingRow = this[rowIdx].scmul(-multiplier)
    var clearedRow = this[i].sum( clearingRow ).clone()
//    console.log("====> cleared " + this[i].toStr() + " to " + clearedRow.toStr())


    this[i] = clearedRow
//    console.log("CLeared one row:\n" + this.toStr())
  }
  return this
}

Array.prototype.pivotize=function(){
  var i=0
  while(this[i] == 0){
    i++
  }
  var pIdx = i
  if(i >= this.length)
    throw new Error("Zero row")

  for(var first=this[i]; i<this.length; i++){
    this[i] /= first
  }
  return pIdx
}


Array.prototype.solve=function(){
  var pivots = []
  for(var i=0; i<this.length; i++){
    pivots[i] = this[i].pivotize()
    this.clearColumn(i)
  }
  //console.log(this.toStr())

  var solutionPoint=[]
  for(var i=0; i<pivots.length; i++){
    solutionPoint[ pivots[i] ] = this[i][ this[i].length -1 ]
  }
  return solutionPoint
}

//Use this vector as a point and, by combining with the direction parameter,
//define a line and return the line representing indices in a linear equation,
//like so:  Ax + By + Cz = D ==> [A, B, C, D]

//for now, only 2D
Array.prototype.toEquationLine=function(dir){
  if(dir.x == 0){ //x is constant and equals this.x
    return [1, 0,  this.x]
  }

  if(dir.y == 0){ //y is constant and equals this.y
    return [0, 1,  this.y]
  }

  return [dir.y/dir.x, -1, -( this.y + (-this.x/dir.x) * dir.y ) ]
}

//2D only for now
Array.prototype.getPerpendicular=function(){
  return [this.y, -this.x].normalized
}

//var mat = [
//  [3, 5,  6, 12],
//  [2, 14, 2, 1 ],
//  [3, 6,  3, 9 ]
//]

var mat = [
  [3, 5,  6,  12 ],
  [6, 10, 12, 24 ],
  [3, 6,  3,  9  ]
]
//console.log("Solving:\n" + mat.toStr())
//console.log("Solved:\n" + mat.solve().toStr())
//console.log("Clearing first col:\n" + mat.clearColumn(0).toStr())









