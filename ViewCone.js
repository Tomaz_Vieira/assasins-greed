function ViewCone(pos, direction, fov){
  this.pos = pos
  this.direction = direction
  this.fov = fov

  this.dirRight = this.direction.rotate(this.fov/2)
  this.dirLeft = this.direction.rotate(-this.fov/2)

  var verts = [new Vertex(this.pos)]
  verts.push( new Vertex( this.pos.sum(this.dirRight) ) )
  verts.push( new Vertex( this.pos.sum(this.dirLeft) ) )


  Triangle.call(this, verts, {strokeStyle: "#FFFF00", fillStyle: "rgba(255,255,0,0.1)"})
}

ViewCone.prototype = Object.create(Triangle.prototype)

ViewCone.prototype.isWithinFOV=function(v){
  var ang = this.direction.angle( v.co.minus(this.pos) )
  return ang < this.fov/2
}

//this is for the radial approach
//ViewCone.prototype.contains=function(v){
//   return this.isWithinFOV(v) && this.pos.distanceTo(v.co) < this.direction.magnitude
//}

ViewCone.prototype.canSeeVert=function(/*Vertex*/ v){
  if(! this.WrapsVert(v) ){
    return false
  }
  for(var i=0; i<this.shadowCones.length; i++){
    if(this.shadowCones[i].WrapsVert(v)){
      return false
    }
  }

  return true
}


//this assumes then edge is fully conained in the view cone
ViewCone.prototype.createShadowCone=function(edge, thresholdEq){
  var thresholdPoints=[]
  for(var i=0; i<2; i++){
    var pt = edge.Vertices[i].co
    var dir = pt.minus(this.pos)
    var line = pt.toEquationLine(dir)
    try{
      var thresholdPt = [
        line,
        thresholdEq.clone()
      ].solve()
      thresholdPoints.push(thresholdPt)
    }catch(e){
      return undefined
    }
  }

  var shadowTris = []
  shadowTris.push(
    new Triangle([
      new Vertex(edge.Vertices[0].co),
      new Vertex(thresholdPoints[0]),
      new  Vertex(edge.Vertices[1].co)
    ])
  )
  shadowTris.push(
    new Triangle([
      new Vertex(edge.Vertices[1].co),
      new Vertex(thresholdPoints[1]),
      new  Vertex(thresholdPoints[0])
    ])
  )
  Game.debugEntities.push(shadowTris[0])
  Game.debugEntities.push(shadowTris[1])

  return shadowTris
}

ViewCone.prototype.update=function(){
    //movement
    if(this.turningLeft){
      //angle is negative because the y canvas axis points DOWN!!
      this.direction = this.direction.rotate(-3)
    }
    if(this.turningRight){ //right
      this.direction = this.direction.rotate(3)
    }
    if(this.movingForward){ //up
      this.pos = this.pos.sum(this.direction.normalized.scmul(2))
    }
    if(this.movingBackwards){ //down
      this.pos = this.pos.sum(this.direction.normalized.scmul(-2))
    }

    Game.debugEntities.push(new Vec(this.pos, this.direction, "#0000FF"))

    this.dirRight = this.direction.rotate(this.fov/2)
    this.dirLeft = this.direction.rotate(-this.fov/2)
    this.verts[0].co = this.pos.clone()
    this.verts[1].co = this.pos.sum(this.dirRight)
    this.verts[2].co = this.pos.sum(this.dirLeft)


    this.rightLine = new Line(this.pos, this.dirRight)
    this.leftLine =  new Line(this.pos, this.dirLeft)

    var viewThresDir = this.verts[2].co.minus(this.verts[1].co)
    this.viewThresholdEq = this.verts[1].co.toEquationLine(viewThresDir)

    this.shadowCones=[]
    for(var i=0; i<Game.entities.length; i++){
      var tri = Game.entities[i]
      if(!(tri instanceof Triangle)){
        continue
      }
      for(var j=0; j<tri.edges.length; j++){
        var e = tri.edges[j]
        var visibleVerts = []
        for(var vIdx=0; vIdx<2; vIdx++){
          if(this.WrapsVert(e.Vertices[vIdx])){
            visibleVerts.push(e.Vertices[vIdx])
          }
        }

        if(visibleVerts.length == 2){
          this.shadowCones = this.shadowCones.concat(
            this.createShadowCone(e, this.viewThresholdEq)
          )
        }else if(visibleVerts.length == 1){
          var pt = e.isInterceptedBy(new Line(this.pos, this.dirRight)) ||
            e.isInterceptedBy( new Line(this.pos, this.dirLeft) )
          if(pt){ //is it ever possible not to have a pt?
            this.shadowCones = this.shadowCones.concat(
              this.createShadowCone(
                new Edge(visibleVerts[0], new Vertex(pt)),
                this.viewThresholdEq
              )
            )
          }
        }
      }
    }

}

/*
ViewCone.prototype.draw=function(){
  Game.context.strokeStyle = "#000000"
  Game.context.beginPath()
  Game.context.moveTo(this.pos.x, this.pos.y)
  
  var arcPt1 = this.pos.sum(this.dirRight  )
  Game.context.lineTo( arcPt1.x, arcPt1.y )
  
  var arcPt2 = this.pos.sum(this.dirLeft)
  Game.context.lineTo(arcPt2.x, arcPt2.y)
  
  Game.context.closePath()
  Game.context.stroke()

  Game.context.fillStyle = "#00FF00"
  Game.context.fillRect(this.pos.x, this.pos.y, 3, 3)

//  console.log("p1: " + arcPt1.toStr())
  Game.context.fillStyle = "#FF0000"
  Game.context.fillRect(arcPt1.x, arcPt1.y, 3, 3)

  Game.context.fillStyle = "#0000FF"
  Game.context.fillRect(arcPt2.x, arcPt2.y, 3, 3)
}
*/
