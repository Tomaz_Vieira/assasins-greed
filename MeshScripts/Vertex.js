﻿function Vertex(co, color){
  /*public Vector3*/ this.co = co.clone();
  /*public Vector2*/ this.uv=[];
  if(color != undefined){
    this.color = color
  }else{
    this.color = "#000000";
  }
  
  /*public CustomMesh*/ this.parentMesh;
  /*public List<Edge>*/ this.Edges=[];
  /*public uint*/ this.processStatus=0;
  /*public int*/ this.index;
  this.visiParsed=false
}
  
/*float*/ Vertex.prototype.getDistanceToVert=function(/*Vertex*/ v){
  return this.co.distanceTo(v.co);
}

/*Edge*/ Vertex.prototype.getEdgeOtherThan=function(edge){
  if(this.Edges.length > 2){
    throw new Error("You're assuming a vert only connected to two edges!"  +
                    "This one has " + this.Edges.length)
  }
  if(this.Edges[0] == edge)
    return this.Edges[1]

  if(this.Edges[1] == edge)
    return this.Edges[0]

  throw new Error("That edge is not connected to this vert")
}
  
/*Edge*/ Vertex.prototype.IsConnectedTo=function(/*Vertex*/ v){
  for(var i=0; i<this.Edges.length; i++){
    var e = this.Edges[i]
    for(var j=0; j<e.Vertices.length; j++){
    var vert = e.Vertices[j]
      if(v == vert){
        return e;
      }
    }
  }
  return null;
}
  
/*Edge*/ Vertex.prototype.Connect=function( /*Vertex*/ v ){
  var e = this.IsConnectedTo(v);
  if(  e != null){
    return e;
  }
  //this will also create the reverse edge
  //on the other vert
  e = new Edge(this, v);
  this.Edges.push( e );
  v.Edges.push(e);
  return e;
}
  
Vertex.prototype.Disconnect=function(/*Vertex*/ vertToDisconnect, /*bool*/ propagate/*=true*/){  
  if(propagate == undefined){
    propagate=true
  }
  for(var i=0; i<this.Edges.length; i++){
    var edg = this.Edges[i]
    for(var j=0; j<edg.Vertices.length; j++){
      var v = edg.Vertices[j]
      if(v == vertToDisconnect){
        this.Edges.Remove(edg);
        if (propagate){
          vertToDisconnect.Disconnect(this, false);
        }
        break;
      }
    }
  }
}

Vertex.prototype.update=function(){
  if(this.debugView){
    if(Game.mainCone.canSeeVert(this) ){
      this.color = "#FF0000"
    }else{
      this.color = "#00FF00"
    }
  }
}

Vertex.prototype.draw=function(){
  Game.context.fillStyle = this.color
  Game.context.fillRect(this.co.x, this.co.y, 3, 3)
}

Vertex.prototype.isSeenBy=function(viewCone){
  return viewCone.canSeeVert(this)
}

//I'm assuming this is  being called on a vertex that is being seen
//  left O----O---O----O   right
//       \   |  /
//        \  | /
//         \ |/
//           x _ _ _ _ _ _ _ _ _right
//   viewPoint
//
Vertex.prototype.createShadowCone=function(viewcone){
  //maybe cone.direction is not the best vector to use here.
  //maybe this.co - viewcone.pos would work better?
  var rightNorm = viewcone.direction.rotate(90) //canvas y points down, so positive angle
  Game.debugEntities.push(new Line(this.co, rightNorm, '#FFFF00'))
  Game.debugEntities.push(new Vec(this.co, rightNorm, '#FFFF00'))

  var rightmostVert = this
  var leftmostVert = this
  var max = -9999999999999
  var min = 999999999999999

  

  //lets get the rightmost first?A
  var triverts = rightmostVert.Edges[0].Triangles[0].verts
  for(var i=0; i < triverts.length; i++){
    var v = triverts[i]
    var v_ang = v.co.minus(viewcone.pos).angle(viewcone.direction)
    console.log("ang: " +  v_ang)
    if( v_ang  > max ){
      max = v_ang
      rightmostVert = v
    }else  if(v_ang  < min){
      min = v_ang
      leftmostVert  =  v
    }else{
      break
    }
    Game.debugEntities.push(new Vertex(rightmostVert.co, "#00FFFF"))
    Game.debugEntities.push(new Vertex(leftmostVert.co, "#FF00FF"))
  }

  return [rightmostVert, leftmostVert]
}

//I'm not sutre where I was going with this. I don't think this function is
//ready, though

/*
  public bool AreVerticesAligned(Vertex v1, Vertex v2){
    Vector3 direction = v1.co - this.co;
    float ratioX = v2.co.x / direction.x;
    float ratioY = v2.co.y / direction.y;
    float ratioZ = v2.co.z / direction.z;
    
    
    MonoBehaviour.print(
      string.Format("Ratios: {0:000.000}  {1:000.000}  {2:000.000}", ratioX, ratioY, ratioZ)
    );
    if( FloatUtils.AreEqual(ratioX, ratioY)){
      if( FloatUtils.AreEqual(ratioY, ratioZ)){
        return true;
      }
    }
    return false;
  }
}
*/
