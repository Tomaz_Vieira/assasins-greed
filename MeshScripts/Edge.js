﻿function Edge(/*Vertex*/ vert1, /*Vertex*/ vert2){
  this.Triangles = []/*new List<Triangle>()*/;
  this.Vertices = [] /*new Vertex[2]*/;
  this.Vertices[0] = vert1;
  this.Vertices[1] = vert2;
}

Object.defineProperty(
  Edge.prototype,
  "isExternal",
  {
    get: function(){
      return this.Triangles.length <= 1
    }
  }
)

Edge.prototype.getDirection=function(){
  return this.Vertices[1].co.minus(this.Vertices[0].co)
}

Edge.prototype.toEquationLine=function(){
  return this.Vertices[0].co.toEquationLine(this. getDirection())
}

Edge.prototype.isInterceptedBy=function(line){
  try{
    var solution=[
      line.toEquationLine(),
      this.toEquationLine()
    ].solve()
    if( isBetween(solution.x, this.Vertices[0].co.x, this.Vertices[1].co.x) ){
      return solution
    }
  }catch(e){
    console.log("paralel lines?")
    console.log(e)
  }
  return false
}

Edge.prototype.getVertOtherThan=function(v){
  if(this.Vertices[0] == v)
    return this.Vertices[1]
  return this.Vertices[0]
}

Edge.prototype.IsPartOfTriangle=function(/*Triangle*/ in_tri){
  for(var i=0; i<this.Triangles.length; i++ ){
    var t = this.Triangles[i]
    if(t == in_tri){
      return true;
    }
  }
  return false;
}

Edge.prototype.HasVertex=function(/*Vertex*/ in_vert){
  for(var i=0; i<this.Vertices.length; i++){
    var v = this.Vertices[i]
    if(v == in_vert){
      return true;
    }
  }
  return false;
}

Edge.prototype.AddTriangle=function(/*Triangle*/ in_tri){
  if(! this.IsPartOfTriangle( in_tri )){
    this.Triangles.push(in_tri);
  }
}






