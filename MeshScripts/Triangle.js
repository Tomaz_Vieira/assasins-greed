﻿function Triangle(/*Vertex[]*/ in_verts, drawStyle){
  this.verts=[];
  this.edges=[];
  this.processStatus=0;
  this.Initialize(in_verts);
  if(drawStyle && drawStyle.strokeStyle ){
    this.strokeStyle = drawStyle.strokeStyle
  }else{
    this.strokeStyle = "rgba(0,0,0,0.1)"
  }

  if(drawStyle && drawStyle.fillStyle ){
    this.fillStyle = drawStyle.fillStyle
  }else{
    this.fillStyle = "rgba(0,0,0,0.1)"
  }

}

Triangle.createFromEdgeAndVert=function(/*Edge*/ in_edge, /*Vertex*/ in_vert){
  if( in_edge.Triangles.length > 1 ){
    throw new Error("trying to append triangle to edge connected to more than 1 tri");
  }
  
  if( in_edge.Triangles.length == 0 ){
    var verts = [] /*new Vertex[3]*/;
    verts[0] = in_edge.Vertices[0];
    verts[1] = in_edge.Vertices[1];
    verts[2] = in_vert;
    return new Triangle(verts);
  }
  
  var tri_base = in_edge.Triangles[0];
  var vertices = []/*new Vertex[3]*/;
  for(var i=0; i<3; i++){
    var triVert = tri_base.verts[i];
    if(in_edge.HasVertex( triVert )){
      vertices[2-i] = triVert;
    }else{
      vertices[2-i] = in_vert;
    }
  }
  return new Triangle(vertices);
}

Object.defineProperty(
  Triangle.prototype,
  "uvs",{
    set: function(value){
      if( ! value)
        return;
      for(var i=0; i<3; i++){
        this.verts[i].uv = value[i];
      }
    }
  }
)
 
/*bool*/Triangle.prototype.WrapsVert=function(/*Vertex*/ v){
//  console.log("I think this needs some checking.");
//  console.log("  Colinear verts might pass the test.");
  var angle=0;
  for(var i=0; i<3; i++){
    angle += this.verts[i].co.minus(v.co).angle(
      this.verts[(i + 1) % 3].co.minus(v.co)
    )
  }

  if(  almostEqual(angle, 360) ){
    return true;
  }else{
    return false;
  }
}
  
/*Vertex*/Triangle.prototype.getVert=function(/*Vector3*/ co){
  for(var i=0; i<this.verts.length; i++){
    var vert = this.verts[i]
    if( vert.co.equals(co) ){
      return vert;
    }
  }
  throw new Error("Are you sure you're looking for the right vert?");
}

Triangle.prototype.Initialize=function(/*Vertex[]*/ in_verts){
  this.verts = in_verts;
  this.connectVerts();
}

Triangle.prototype.connectVerts=function(){
  this.edges = []/*new Edge[3]*/;
  for(var i=0; i<3; i++){
    var e = this.verts[i].Connect( this.verts[ (i+1) % 3 ] );
    this.edges[i] = e;
    e.AddTriangle(this);
  }
}
  
//  public Triangle appendTriangle(Vertex in_vert){
//    float maxDistance = float.MinValue;
//    Vertex furthestVert=null;
//    foreach(Vertex v in this.verts){
//      float dist = v.getDistanceToVert(in_vert);
//      if( dist > maxDistance ){
//        maxDistance = dist;
//        furthestVert = v;
//      }
//    }
//    
//    foreach(Edge e in this.edges){
//      if( ! e.HasVertex( furthestVert ) ){
//        return new Triangle( e, in_vert);
//      }
//    }
//    
//    throw new UnityException("This should never  happen.");
//  }

Object.defineProperty(
  Triangle.prototype,
  "Normal",
  {
    get: function(){
      var v1 = verts[1].co - verts[0].co
      var v2 = verts[2].co - verts[0].co
      return v1.cross(v2).normalized ;
    }
  }
)
  
Triangle.prototype.FlipNormal=function(){
  var v;
  v = this.verts[0];
  this.verts[0] = this.verts[2];
  this.verts[2] = v;
}
  
//  public Triangle ExtrudePlane( Vector3 direction, float distance ){
//    Vertex[] newVertices = new Vertex[3];
//    for(int i=0; i<3; i++){
//      newVertices[i] = this.verts[i].CloneVert();
//      newVertices[i].co += distance*direction;
//      newVertices[i].Connect(this.verts[i]);
//    }
//    Triangle tri = new Triangle( newVertices );
//    return tri;
//  }
  
/*Edge*/Triangle.prototype.FindEdge=function(/*Vertex*/ v1, /*Vertex*/ v2){
  for(var i=0; i<this.edges.length; i++){
    var e = this.edges[i]
    if( e.HasVertex(v1) && e.HasVertex(v2) )
      return e;
  }
  return null;
}

Object.defineProperty(
  Triangle.prototype,
  "Area",
  {
    get: function(){
      var v1 = verts[1].co - verts[0].co
      var v2 = verts[2].co - verts[0].co
      return v1.cross(v2).magnitude / 2 ;
    }
  }
)


//Delete this
Triangle.prototype.update=function(){
  for(var i=0; i<this.verts.length; i++){
    this.verts[i].update()
  }
}

//this is just for debugging. Triangles should prolly be drawn by
//their mesh objecs, which would know their position in the world.
//I don't think it makes sense to have tris with global coordinates
Triangle.prototype.draw=function(){
  var pt1 = this.verts[0].co
  var pt2 = this.verts[1].co
  var pt3 = this.verts[2].co

  Game.context.strokeStyle = this.strokeStyle
  Game.context.fillStyle = this.fillStyle
  Game.context.beginPath()
  Game.context.moveTo( this.verts[0].co.x, this.verts[0].co.y)
  
  Game.context.lineTo( this.verts[1].co.x, this.verts[1].co.y )
  Game.context.lineTo( this.verts[2].co.x, this.verts[2].co.y)
  
  Game.context.closePath()
  Game.context.stroke()
  Game.context.fill()

  for(var i=0; i<3; i++){
    this.verts[i].draw()
  }

}
