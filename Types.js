function isVectorArray(x){
  if( ! (x instanceof Array)  ){
    return false
  }

  for(var i=0; i<x.length; i++){
    if( ! (x[i] instanceof Array) ){
      return false
    }
  }
  return true
}
