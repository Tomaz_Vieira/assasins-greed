"use strict";

function clamp(min, max, val) {
  return Math.min(Math.max(val, min), max);
}

function  isBetween(x,a,b){
  if(x>=a && x<=b)
    return true
  if(x >= b && x<= a)
    return true
    
  return false
}
