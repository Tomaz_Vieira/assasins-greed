function PhysObj(physObjArgs){
  this.init(physObjArgs)
}

PhysObj.prototype.init = function(physObjArgs){
  this.img = physObjArgs.img
  this.entityPool = physObjArgs.entityPool
  this.speed = physObjArgs.speed || [0,0]
  this.dimensions = physObjArgs.dimensions || [100,100]
  this.mass = physObjArgs.mass || 0.1
  this.pos = physObjArgs.pos || [0,0]

  this.dragForceMagnitude = physObjArgs.dragForceMagnitude || 0
  this.color = physObjArgs.color || "#000000"
  this.resultForce = [0,0]
}

PhysObj.prototype.destroy=function(){
  var index = this.entityPool.indexOf(this);
  if (index > -1) {
      this.entityPool.splice(index, 1);
  }
  
  if(this.callback)
    this.callback()
}

PhysObj.prototype.update=function(){
  //deal with drag
  var speedMag = this.speed.magnitude
  var drag = this.speed.normalized.negative.scmul( speedMag * speedMag * this.dragForceMagnitude)
  this.resultForce = this.resultForce.sum( drag )

  var accel = this.resultForce.div(this.mass) // F = m * a -> a = F / m
  var speedDelta = accel.scmul(Game.timeDelta) 
  this.speed =  this.speed.sum( speedDelta ) // dV =  a * dT

  if (Math.abs(this.speed) < 0.001){
    this.speed = 0
  }

  var posDelta = this.speed.scmul( Game.timeDelta )
  this.pos = this.pos.sum( posDelta )

  //reset forces so they cam be recomposed on the next frame
  this.resultForce = [0,0]

  //destroy object if it leaves the viewport
  if(!this.isColliding(Game.canvasPhysObj)){
    this.destroy()
  }
}

PhysObj.prototype.draw=function(){
  if(this.invisible)
    return

  if(this.img){
    Game.context.drawImage(
      this.img,
      0,0,
      this.img.width, this.img.height,
      this.pos.x, this.pos.y,
      this.dimensions.x, this.dimensions.y
    )
  }else{
    Game.context.fillStyle = this.color
    Game.context.fillRect(this.pos.x, this.pos.y, this.dimensions.x, this.dimensions.y)
  }

}

PhysObj.prototype.isColliding = function(other){
  if( this.pos.x > other.pos.x + other.dimensions.x )
    return false

  if(this.pos.y > other.pos.y + other.dimensions.y)
    return false

  if(this.pos.x + this.dimensions.x < other.pos.x)
    return false

  if(this.pos.y + this.dimensions.y < other.pos.y)
    return false

  return true
}
