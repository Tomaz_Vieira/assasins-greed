function getFrameCoords(img, frameWidth, frameHeight){
  var frameCoords=[]
  for(var lineIdx=0; lineIdx < img.height / frameHeight; lineIdx++){
    for(var colIdx=0; colIdx < img.width / frameWidth; colIdx++ ){
      frameCoords.push(
        {
          offsetX: colIdx*frameWidth,
          offsetY: lineIdx*frameHeight+1,
          width: frameWidth,
          height: frameHeight
        }
      )
    }
  }
  return frameCoords
}

function Sprite(img, frameWidth, frameHeight, frameTime, callback){
  this.img=img
  this.callback=callback
  this.frames = getFrameCoords(img, frameWidth, frameHeight)
  this.frameTime = frameTime
  this.currFrame=0
  this.momentFired = (new Date).getTime()
}

Sprite.prototype = Object.create(PhysObj.prototype)

Sprite.prototype.fire=function(){
  this.momentFired = (new Date).getTime()
}

Sprite.prototype.draw=function(){
  var frame = this.frames[this.currFrame]
  Game.context.drawImage(
    this.img,
    frame.offsetX,
    frame.offsetY,
    frame.width,
    frame.height,
    this.pos.x,
    this.pos.y,
    frame.width,
    frame.height
//    this.pos.x,
//    this.pos.y
  )
}

Sprite.prototype.update=function(){
  PhysObj.prototype.update.call(this)
  var elapsedTime = (new Date).getTime() - this.momentFired
  if(  elapsedTime > this.frames.length * this.frameTime ){
    if(this.callback){
      this.callback()
    }
    this.destroy()
  }
  this.currFrame = Math.min(
    this.frames.length - 1,
    parseInt(elapsedTime / this.frameTime)
  )
}




