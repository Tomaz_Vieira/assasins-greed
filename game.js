var Game = {};

Game.fps = 30;

Game.timeDelta = 1/Game.fps



function distSorterFactory(origin){
  function compDist(a,b){
    return a.distanceTo(origin) - b.distanceTo(origin)
  }

  return compDist
}

//returns the closes edge found by the trace
//this is absolutely unoptimized. Needs max dist as well
Game.traceRay=function(origin, dir){
  Game.debugEntities.push(new Line(origin, dir))

  var edges = []
  for(var i=0; i<this.entities.length; i++){
    var ent = this.entities[i]
    if(ent instanceof Edge){
      edges.push(ent)
    }else if( ent instanceof Triangle ){
      for(var j=0; j<ent.edges.length; j++){
        edges.push(ent.edges[j])
      }
    }
  }

  var closestPoint  = undefined
  var closestEdge = undefined
  var minDist = 99999
  for(var i=0; i<edges.length; i++){
    var p = edges[i].isInterceptedBy(new Line(origin, dir))
    if(!p)
      continue
    var dst = p.distanceTo(origin)
    if(dst < minDist){
      minDist =  dst
      closestPoint = p
      closestEdge = edges[i]
    }
  }

  if(closestPoint){
    return {pt: closestPoint, edge: closestEdge }
  }
}

Game.initialize = function() {
  this.sprites = []
  this.entities = [];
  this.debugEntities = []

  this.collisionEntities = []


  this.canvas =  document.getElementById("canvas")
  this.context = this.canvas.getContext("2d");
  //spawn player car
 
  this.canvasPhysObj = new PhysObj({
    pos: [0,0],
    dimensions: [this.context.canvas.width, this.context.canvas.height],
    color: "#CCCCCC"
  })

  this.mainCone =  new ViewCone([300,300], [-100,-100], 45) 
  this.entities.push(
    new Triangle(
      [
        new Vertex([150, 150]),
        new Vertex([170, 130]),
        new Vertex([130, 140])
      ],
      {
        fillStyle: "#FF0000"
      }
    )
  )
  this.entities.push(
    new Triangle(
      [
        new Vertex([200, 120]),
        new Vertex([220, 110]),
        new Vertex([180, 120])
      ],
      {
        fillStyle: "#FF0000"
      }
    )
  )
  var v = new Vertex([200,150])
  v.debugView = true
  this.entities.push(v)


  document.addEventListener('keydown', function(e){
    var cone = Game.mainCone
    switch(e.keyCode){
      case 37: //left
        e.preventDefault()
        cone.turningLeft = 1
        break
      case 39: //right
        e.preventDefault()
        cone.turningRight = 1
        break
      case 38: //up
        e.preventDefault()
        cone.movingForward = 1
        break
      case 40: //down
        e.preventDefault()
        cone.movingBackwards = 1
        break
    }
  })

  document.addEventListener('keyup', function(e){
    var cone = Game.mainCone
    switch(e.keyCode){
      case 37: //left
        e.preventDefault()
        cone.turningLeft = 0
        break
      case 39: //right
        e.preventDefault()
        cone.turningRight = 0
        break
      case 38: //up
        e.preventDefault()
        cone.movingForward = 0
        break
      case 40: //down
        e.preventDefault()
        cone.movingBackwards = 0
        break
    }
  })


};


Game.draw = function() {
  this.context.clearRect(0, 0, 800, 600);


  this.canvasPhysObj.draw()

  this.mainCone.draw()

  for(var i=0; i<this.entities.length; i++){
    this.entities[i].draw()
  }
  for(var i=this.collisionEntities.length -1; i>=0 ; i--){
    this.collisionEntities[i].draw()
  }

  for(var i=0; i<this.sprites.length; i++){
    this.sprites[i].draw()
  }

  for(var i=0; i<this.debugEntities.length; i++){
    this.debugEntities[i].draw()
  }

};


Game.update = function() {
  this.debugEntities = []

  this.mainCone.update()

  for(var i=0 ; i<this.collisionEntities.length; i++){
    this.collisionEntities[i].update()
  }

  //update non-coliding stuff
  for(var i=0; i<this.entities.length; i++){
    this.entities[i].update()
  }

  //update sprites
  for(var i=0; i<this.sprites.length; i++){
    this.sprites[i].update()
  }
};


Game.spawnEnemyHazard = function(){
  if( this.totalDistance - this.lastHazardSpawn >  Game.hazardSpawnInterval){
    Game.lastHazardSpawn = this.totalDistance
    var r = Math.random()
    if(r > 0.4){
      this.collisionEntities.push(new HazardCar())
    }else if (r > 0.2){
      this.collisionEntities.push(new OilHazard())
    }else{
      this.collisionEntities.push(new HoleHazard())
    }
  }
}
